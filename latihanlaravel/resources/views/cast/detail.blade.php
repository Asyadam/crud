@extends('layout.master')

@section('judul')
HALAMAN detail CAST
@endsection

@section('content')
<h1>NAMA : {{ $cast->nama }}</h1>
<p>UMUR: {{ $cast->umur }}</p>
<p>BIODATA: {{ $cast->bio }}</p>

<a href="/cast" class="btn btn-danger btn-sm"> kembali</a>
@endsection
