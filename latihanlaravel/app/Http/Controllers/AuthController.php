<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('page.register');
    }

    public function welcome(Request $request) {
        $first = $request->input('firstName');
        $last = $request->input('lastName');

        return view('page.welcome', ['first' => $first, 'last' => $last]);
    }
}
